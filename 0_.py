import csv
import json
import time
import aiohttp
import asyncio

from bs4 import BeautifulSoup


async def get_url(url):
    async with aiohttp.ClientSession() as session:
            response = await session.get(url=url+'/about')
            try:
                soup = BeautifulSoup(await response.text(), 'lxml')
            except:
                soup = ''
            try:
                items  = soup.find_all('script')
            except:
                items = ''
            try:
                need_item = items[36].text.replace('var ytInitialData =', '').replace('}}}}]};', '}}}}]}')
                json_data = json.loads(need_item)
            except Exception as ex:
                    json_data =''    
            try:
                title = json_data['metadata']['channelMetadataRenderer']['title']
            except:
                title =''
            try:
                link_channel = json_data['metadata']['channelMetadataRenderer']['channelUrl'] 
            except:
                link_channel = ''
            with open('base.csv', encoding='utf-8') as csvfile:
                reader = csv.DictReader(csvfile, delimiter=',')
                for row in reader:
                    link = row['url']
                    mail = row['email']
                    if link_channel in link:
                        with open(f"data.csv", "+a", encoding='utf-8', newline='') as file:
                                                    writer = csv.writer(file, delimiter=';')

                                                    writer.writerow(
                                                        (   link,
                                                            url,
                                                            mail,
                                                            title
                                                        

                                                    ))
                        break

async def main():
    url = input('Url: ')
    await get_url(url)
    
    
if __name__ == '__main__':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main())
